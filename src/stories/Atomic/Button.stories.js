import * as React from 'react';
import {action} from "@storybook/addon-actions"

import Btn from '../../atoms/Button';
import Picture from '../../atoms/Picture';
import TextData from '../../atoms/TextData';

import addIcon from "../../_settings/img/icons/add.svg";

export default {
  title: 'Atomic/Button/Simple_Boutton',
  component: Btn,
};

const Template = (args)=> <Btn {...args}></Btn>
export const SimpleBtn = Template.bind({});

SimpleBtn.args = {
  title:"boutton",
  id:"id_btn",
  width:"218px",
  height:"40px",
  padding:"7px",
  className:["bg-cyan-500","rounded-md"],
  onClick:action("Simple Btn is Clicked"),
  children:(
    <>
      <Picture width="24px" height="24px" imgPath={addIcon}/>
      <TextData variant="regular" size={14} className={["text-white"]}>Créer un nouveau contrat</TextData>
    </>
  ),
}


