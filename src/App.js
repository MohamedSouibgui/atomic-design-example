import './App.scss';
import TextData from './atoms/TextData';
import Picture from './atoms/Picture';
import Btn from './atoms/Button';
import Input from './atoms/Input';

import addIcon from './_settings/img/icons/add.svg';
import simpleIcon from './_settings/img/fr-flag.svg';
import Checkbox from './atoms/Checkbox';

function App() {
  return (
    <div style={{width:'400px' ,marginLeft:'100px',marginTop:'100px', backgroundColor:'#bdc3c7'}}>
      <TextData></TextData>
      <input type="text" id="test" style={{border:"2px solid red"}}></input>
      <hr/>
      <Picture/>
      <hr/>
      
      <Btn className={["bg-cyan-500","rounded-md"]} width="218px" height="40px" padding="7px">
        <Picture width="24px" height="24px" imgPath={addIcon}/>
        <TextData variant="regular" size={14} className={["text-white"]}>Créer un nouveau contrat</TextData>
      </Btn>
      <hr/><hr style={{margin:'20px'}}/>
      <Checkbox id="1">
        test
      </Checkbox>
      <hr style={{margin:'20px'}}/>
      <Input id="7">
      </Input>
    </div>
  );
}

export default App;
