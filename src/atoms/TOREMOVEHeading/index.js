import * as React from 'react'
import classnames from 'classnames'
import PropTypes from 'prop-types';

import './style.css';

const Heading = ({children,onClick,title,className,id,size}): React.Element<*> => {
  if(!title && typeof children === "string"){
    title = children;
    console.warn("The title prop is not provided");
  } 
  const classProps: string = classnames(
    
    className,"heading"
  )
  return (
    <h1 onClick={onClick} title={title} 
    className={classProps} id={id} 
      style={{
        fontSize:size}}>
      {children}
    </h1>
  )
}

Heading.propTypes = {
  children:React.Node,
  onClick:PropTypes.func,
  title: PropTypes.string,
  className:PropTypes.string,
  id:PropTypes.number,
  size:PropTypes.number,
}

Heading.defaultProps = {
  children:"null",
  title:null,
  id:null,
  onClick:()=>{console.log("styles.heading")},
  size:16
}

export default Heading;