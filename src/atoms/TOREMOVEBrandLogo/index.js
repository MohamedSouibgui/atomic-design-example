/* eslint-disable no-unused-vars */
import * as React from 'react'
import classnames from 'classnames'
import PropTypes from 'prop-types';

import BrandLogoSVG from "../../_settings/img/logo.svg";
import WhiteBrandLogoSVG from "../../_settings/img/w-logo.svg";

import './style.css';

const BrandLogo = ({onClick,imgPath,title,alt,className,id,width,height}): React.Element<*> => {
  if(!title){
    console.warn("The title prop is not provided");
  } 
  const classProps: string = classnames(
    className,"span"
  )
  return (
    <img  src={imgPath} alt={alt} onClick={onClick} title={title} 
      className={classProps} id={id} style={{width:width,height:height}}/>
  )
}

BrandLogo.propTypes = {
  imgPath:PropTypes.string,
  title: PropTypes.string,
  alt: PropTypes.string,
  className:PropTypes.string,
  id:PropTypes.number,
  width:PropTypes.string,
  height:PropTypes.string,
  onClick:PropTypes.func,
}

BrandLogo.defaultProps = {
  imgPath:WhiteBrandLogoSVG,
  title: "brand",
  alt: "brand",
  className:["brandLogo"],
  id:null,
  width:"300px",
  height:"200px",
  onClick:()=>{console.log("styles.heading")},
}

export default BrandLogo;