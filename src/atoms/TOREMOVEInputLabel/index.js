import * as React from 'react'
import classnames from 'classnames'
import PropTypes from 'prop-types';

import './style.css';

const InputLabel = ({children,forId,onClick,title,className,id,size}): React.Element<*> => {
  if(!title && typeof children === "string"){
    title = children;
    console.warn("The title prop is not provided");
  } 
  const classProps: string = classnames(
    className,"label"
  )
  return (
    <label for={forId} onClick={onClick} title={title} 
    className={classProps} id={id} 
      style={{fontSize:size}}>
      {children}
    </label>
  )
}

InputLabel.propTypes = {
  children:React.Node,
  forId:PropTypes.string,
  onClick:PropTypes.func,
  title: PropTypes.string,
  className:PropTypes.string,
  id:PropTypes.number,
  size:PropTypes.number,
}

InputLabel.defaultProps = {
  children:"null",
  forId:null,
  title:null,
  id:null,
  onClick:()=>{console.log("styles.heading")},
  size:16
}

export default InputLabel;