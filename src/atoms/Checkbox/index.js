import * as React from 'react';
import classnames from 'classnames';

import './style.scss'

const Checkbox = (props:CheckboxProps):FC<*>=>{
  if(!props.id){
    return null;
  }
  const divClassNames = classnames(
    "checkbox-container",
  )
  const labelClassNames = classnames(
    props.className,
  )
  // document.documentElement.style.setProperty('--testing','#aaa');
  let result = (
    <div className={divClassNames} title={props.title}>
      <input type="checkbox" id={props.id}  title={props.title}/>
      <label 
        className={labelClassNames} 
        htmlFor={props.id} 
        style={{...props.style,fontSize:props.textSize,color:props.textColor}}
        title={props.title}>
          {props.text}
      </label>
    </div>
  )
  return result;

}
interface CheckboxProps {
  text:string;
  textSize:string;
  textColor:String;
  title:string;
  id:string;
  className:string;
}

Checkbox.defaultProps = {
  text:"string",
  textSize:"18px",
  textColor:"var(--t-600)",
  title:"string",
  id:null,
  className:["string"],
}
export default Checkbox;