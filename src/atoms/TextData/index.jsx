import * as React from 'react'
import classnames from 'classnames'

import './style.css';

const TextData = (props:TextDataProps): FC<*> => {
  let title = props.title;
  if(!title && typeof props.children === 'string'){
    title = props.children;
  }
  
  const classProps: string = classnames(props.className, props.variant);
  
  let CustomTag:string;
  switch(props.variant){
    case "heading":
      CustomTag = 'h1';
      break;
    case "regular":
      CustomTag = 'span';
      break;
    case "label":
      CustomTag = 'label';
      break;
    default:
      CustomTag = 'div';
  }
  return (
    <CustomTag
      htmlFor={props.forId}
      onClick={props.onClick}
      title={title}
      className={classProps}
      id={props.id}
      style={{
        fontSize: parseInt(props.size),
        ...props.style,
      }}
    >
      {props.children}
    </CustomTag>
  )
}

interface TextDataProps {
  children: React.Node;
  variant:'heading' | 'regular' | 'label';
  title:string;
  id:string;
  forId:string;
  size:string;
  className:string;
  onClick:Function;
}

TextData.defaultProps = {
  children:"This is TextData Atome",
  variant:"heading",
  title:null,
  id:null,
  forId:"0",
  size:null,
  className:[],
  onClick:()=>{console.log("onClick Func")},
}

export default TextData;