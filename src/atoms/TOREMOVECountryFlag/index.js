/* eslint-disable no-unused-vars */
import * as React from 'react'
import classnames from 'classnames'
import PropTypes from 'prop-types';

import FlagFR from "../../_settings/img/fr-flag.svg";
import FlagES from "../../_settings/img/es-flag.svg";

import './style.css';

const CountryFlag = ({onClick,imgPath,title,alt,selected,className,id,width,height}): React.Element<*> => {
  if(!title){
    console.warn("The title prop is not provided");
  } 
  const classProps: string = classnames(
    selected?"animate-bounce":null,
    className,"flag"
  )
  return (
    <img  src={imgPath} alt={alt} onClick={onClick} title={title} 
      className={classProps} id={id} style={{width:width,height:height}}/>
  )
}

CountryFlag.propTypes = {
  imgPath:PropTypes.string,
  title: PropTypes.string,
  alt: PropTypes.string,
  selected:PropTypes.bool,
  className:PropTypes.string,
  id:PropTypes.number,
  width:PropTypes.string,
  height:PropTypes.string,
  onClick:PropTypes.func,
}

CountryFlag.defaultProps = {
  imgPath:FlagFR,
  title: "brand",
  alt: "brand",
  selected:false,
  className:[],
  id:null,
  width:"300",
  height:"200",
  onClick:()=>{console.log("styles.heading")},
}

export default CountryFlag;