/* eslint-disable no-unused-vars */
import * as React from 'react'
import classnames from 'classnames'

import FlagFR from "../../_settings/img/fr-flag.svg";

import './style.css';

const Picure = (props:PictureProps): FC<*> => {
  let title = props.title;
  if(!title && typeof props.children === 'string'){
    title = props.children;
  }

  const classProps: string = classnames(
    'Picture',
    props.className, 
    props.bounce?"animate-bounce":null,
  );
  
  return (
    <img  
      src={props.imgPath} 
      title={props.title} 
      alt={props.alt} 
      className={classProps} 
      id={props.id} 
      style={{...props.style,width:props.width,height:props.height}}
      onClick={props.onClick} 
    />
  )
}

interface PictureProps {
  imgPath:string;
  title:string;
  alt:String;
  bounce:boolean;
  id:string;
  width:String;
  height:string;
  className:string;
  onClick:Function;
}

Picure.defaultProps = {
  imgPath:FlagFR,
  title: "img",
  alt: "img",
  bounce:false,
  id:null,
  width:"300",
  height:"200",
  className:[],
  onClick:()=>{},
}

export default Picure;