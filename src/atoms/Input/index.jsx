import * as React from 'react';
import classnames from 'classnames';

import TextData from '../TextData';
import Picture from '../Picture';

import addIcon from '../../_settings/img/icons/add.svg';

import './style.scss'

const RATIO_SIZE = 0.8; //Determine the size of caption and error text. 

const InputCaption = (props:InputProps):FC<*>=>{
  const [textSize] = React.useState(parseInt(props.textSize) * RATIO_SIZE);
  if(props.inputCaption && props.inputCaption.length>0){
    return(
    <TextData 
      variant="label" 
      forId={props.id} 
      size={textSize}
      style={{
        color:props.textColor,
      }}
    >
      {props.inputCaption}
    </TextData>
    );
  }else{
    return null;
  }
}
const InputError = (props:InputProps):FC<*>=>{
  const [textSize] = React.useState(parseInt(props.textSize) * RATIO_SIZE);
  if(props.errorText && props.errorText.length>0){
    return(
    <TextData 
      variant="label" 
      forId={props.id}
      size={textSize}
      style={{
        color:props.errorColor,
      }}
    >
      {props.errorText}
    </TextData>
    );
  }else{
    return null;
  }
}
const Icon = (props:InputProps):FC<*>=>{
  const className = ["input-icon", props.children?.props.className];
  return props.children ? (
    <>
      {React.cloneElement(props.children, {
        className,
        onClick: props.iconClickable ? props.iconClickHandler : undefined,
      })}
    </>
  ) : (
    <Picture
      className={className}
      imgPath={props.iconImg}
      onClick={props.iconClickable ? props.iconClickHandler : undefined}
    />
  );
}
const InputField = (props:InputProps):FC<*>=>{
  const styles = {
    width: parseInt(props.inputWidth),
    borderWidth: parseInt(props.borderWidth),
    borderColor:props.borderColor,
    borderRadius: parseInt(props.inputRadius),
    paddingLeft: props.iconImg && props.iconLocation === "left" ? 30 : 5,
    paddingRight: props.iconImg &&  props.iconLocation === "left" ? 5 : 30,
  };
  return (
    <div 
      className="input-container" 
      style={{
        width:parseInt(props.inputWidth),
        flexDirection: props.iconLocation === "left" ? "row" : "row-reverse",
      }}
    >
      <input 
        id={props.id} 
        className={props.classProps}
        style={styles}
        placeholder={props.placeholder}
      />
      {props.iconImg ? <Icon {...props}/> : null}
    </div>
  )
}
const InputItem = (props:InputProps):FC<*>=>{
  if(!props.id){
    return null;
  }
  const classProps: string = classnames(
    'inputItem',
    props.className,
  );

  const result = (
    <div>
      <InputCaption {...props}/>
      <InputField {...props} classProps={classProps}/>
      <InputError {...props}/>
    </div>
  )
  return result;
}
interface InputProps {
  children:string;
  borderWidth:number;
  borderColor:string;
  iconLocation:["left","right"];
  iconImg:string;
  iconClickable:Boolean;
  iconClickHandler:Function;
  inputWidth:string;
  placeholder:string;
  errorText:string;
  errorColor:string;
  inputCaption:string;
  inputRadius:string;
  onSubmit:Function;
  onChange:Function;
  textColor:string;
  textSize:number;
  title:string;
  id:string;
  className:string;
}

InputItem.defaultProps = {
  children:null,
  borderWidth:0,
  borderColor:"var(--t-600)",
  inputRadius:"2px",
  iconLocation:"right",
  iconImg:addIcon,
  iconClickable:true,
  iconClickHandler:()=>{console.log("Icon Clicked")},
  inputWidth:"200px",
  placeholder:"Recherche...",
  errorText:"ceci est un error",
  errorColor:"red",
  inputCaption:"ceci est une description",
  onSubmit:()=>{},
  onChange:()=>{},
  textColor:"var(--v-600)",
  textSize:15,
  title:"ceci est un titre",
  id:null,
  className:[""],
}
export default InputItem;