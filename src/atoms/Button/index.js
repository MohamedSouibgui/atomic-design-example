import * as React from 'react';
import classnames from 'classnames';

import './style.css'

const Btn = (props:ButtonProps):FC<*>=>{
  let defJustCont=true;
  
  props.className.filter(simpleClass => {
    if(simpleClass.includes("justify-")){
      defJustCont = false;
    }
    return simpleClass;
  })
  
  const classNames = classnames(
    {'justify-around':defJustCont},
    'Btn',
    props.className,
  )

  return (
    <button 
      className={classNames}
      title={props.title}
      id={props.id}
      style={{...props.style,width:props.width,height:props.height,padding:props.padding}}
      onClick={props.onClick}
    >
      {props.children}
    </button>
  )

}

interface ButtonProps {
  children:React.Node;
  title:string;
  id:string;
  width:string;
  height:string;
  padding:string;
  className:string;
  onClick:Function;
}

Btn.defaultProps = {
  children:"boutton",
  className:["bg-cyan-500","rounded-md"],
  title:"boutton",
  id:"id_btn",
  width:"218px",
  height:"40px",
  padding:"7px",
}

export default Btn;